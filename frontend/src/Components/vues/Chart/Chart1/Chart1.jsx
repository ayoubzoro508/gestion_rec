
import { PieChart } from '@mui/x-charts/PieChart';
import './chart1.css'
import { useEffect, useState } from 'react';
import { Axios } from '../../../Api/Axios';
import { Divider } from '@mui/material';
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
export default function BasicPie() {
 
  const [CountReclamation,setCountReclamation]=useState("")
  const Countreclamations=async()=>{
 await Axios.get('/api/CountReclamation').then((res)=>setCountReclamation(res.data))
  }
  const [CountReclamationEncours,setCountReclamationEncours]=useState("")
  const CountreclamationEncours=async()=>{
   await Axios.get('/api/CountReclamationEncours').then((res)=>setCountReclamationEncours(res.data))
    }
    const [CountReclamationTraité,setCountReclamationTraité]=useState("")
    const CountreclamationTraité=async()=>{
     await Axios.get('/api/CountReclamationTraité').then((res)=>setCountReclamationTraité(res.data))
      }
  useEffect(()=>{
    
     Countreclamations()
     CountreclamationEncours()
     CountreclamationTraité()
   },[])

   
  return (
    <div className='chart'>
        
    
    <PieChart
      series={[
        {
          data: [
            { id: 0, value: `${CountReclamationEncours}`,color:'#F7D154'  },
            { id: 1, value: `${CountReclamationTraité}` ,color:'#1070CA' },
            
          ],
          highlightScope: { faded: 'global', highlighted: 'item' },
          faded: { innerRadius: 30, additionalRadius: -30, color: 'black' },
          innerRadius: 66,
          outerRadius: 103,
          startAngle: 180,
          endAngle: -180,
          
        },
      ]}
      height={290}
      sx={{position:'relative',top:0,right:0,ml:10}}
    />
    
    <div className="infoschart">
      <Divider sx={{position:'relative',top:-10,width:500}}/>
      <div className='infos'>
        <div className="droit">
          <TaskAltIcon fontSize='large'/>
          <div className='status'>Traitée</div>
          
          <div className="result1">
          {((CountReclamationTraité / CountReclamation  ) * 100).toFixed(0)} %
          </div>
        </div>
        <div className="gauche">
          <AccessTimeIcon fontSize='large'/>
          <div className='status'>En Cours</div>
          
          <div className="result2">
          {((CountReclamationEncours / CountReclamation ) * 100).toFixed(0) } %
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}