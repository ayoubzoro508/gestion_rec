import React , {useState , useEffect} from 'react';
import { BarChart } from '@mui/x-charts/BarChart';
import { Axios } from '../../../Api/Axios';
import './chart2.css'
import Divider from '@mui/material/Divider';

export default function Chart2() {
  const [CountReclamation,setCountReclamation]=useState("")
  const Countreclamations=async()=>{
 await Axios.get('/api/CountReclamation').then((res)=>setCountReclamation(res.data))
  }
  const [CountReclamationEncours,setCountReclamationEncours]=useState("")
  const CountreclamationEncours=async()=>{
   await Axios.get('/api/CountReclamationEncours').then((res)=>setCountReclamationEncours(res.data))
    }
    const [CountReclamationTraité,setCountReclamationTraité]=useState("")
    const CountreclamationTraité=async()=>{
     await Axios.get('/api/CountReclamationTraité').then((res)=>setCountReclamationTraité(res.data))
      }
  useEffect(()=>{
     
     Countreclamations()
     CountreclamationEncours()
     CountreclamationTraité()
   },[])
  const date = new Date();
  const year = date.getFullYear();
  return (
    <div className='chart2'>
      
        <BarChart
        
      xAxis={[{ scaleType: 'band',data: [year - 3, year - 2, year - 1, year] }]}
      
      series={[{ data: [CountReclamationEncours],color:'#F7D154',label:"En Cours"  }, { data: [CountReclamationTraité],color:'#1070CA',label:"Traitée" }]}
      width={700}
      height={300}
      
    />
    <Divider/>
    </div>
  );
}