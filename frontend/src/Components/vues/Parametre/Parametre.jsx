import React from 'react'



import Avatar from '@mui/joy/Avatar';
import '@fontsource/inter';
import Chip from '@mui/joy/Chip';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import ButtonGroup from '@mui/joy/ButtonGroup';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';
import CardActions from '@mui/joy/CardActions';
import IconButton from '@mui/joy/IconButton';
import Typography from '@mui/joy/Typography';
import SvgIcon from '@mui/joy/SvgIcon';

import { Link } from 'react-router-dom';
import { UserContext } from '../../ContextApi/ContexteApi';
import Copyright from '../../Footer/CopyRight';

export default function Parametre() {
  const {user}=UserContext()
  const date = new Date(user.created_at);
  const created = date.toLocaleString()
  return (<>
    <Card
      sx={{
       
        
        
        width: 600,
        height:600,
        maxWidth: '100%',
        boxShadow: 'lg',
        marginTop:"100px",
        marginRight:50,
        display:'flex',
        alignItems:'center',
        bgcolor:'#e9ecef47',
        border:'none',
        backdropFilter:'blur(50px)'
      }}
    >
      <CardContent sx={{ alignItems: 'center', textAlign: 'center' }}>
        <Avatar src="/static/images/avatar/1.jpg" sx={{ '--Avatar-size': '10rem' }} />
        <Chip
          size="sm"
          variant="soft"
          color="primary"
          sx={{
            mt: -1,
            mb: 1,
            border: '1px solid',
            borderColor: 'background.surface',
          }}
        >
          Bienvenue
        </Chip>
        <Typography level="title-lg">{`${user.name}`}</Typography><br />
        <Typography level="body-sm" sx={{ maxWidth: '30ch',color:'black' }}>
        Nom :{user.name}
        </Typography>
        <Typography level="body-sm" sx={{ maxWidth: '30ch',color:'black' }}>
        Email :{user.email}
        </Typography>
        <Typography level="body-sm" sx={{ maxWidth: '30ch',color:'black' }}>
        Ajouter Le :{created}
        </Typography>
        <Box
          sx={{
            display: 'flex',
            gap: 2,
            mt: 2,
            '& > button': { borderRadius: '2rem' },
          }}
        >
          
        </Box>
      </CardContent>
      <CardOverflow sx={{ bgcolor: 'background.level1' }}>
        <Link to={`/EditProfile/${user.id}`}><CardActions buttonFlex="1">
          <ButtonGroup variant="outlined" sx={{ bgcolor: 'background.surface',m:3 }}>
           <Button >Modifier</Button>  
            
          </ButtonGroup>
        </CardActions></Link>
      </CardOverflow>
      
    </Card>
    </>
  );
}
