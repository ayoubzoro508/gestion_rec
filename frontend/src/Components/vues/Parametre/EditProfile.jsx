
import Avatar from '@mui/joy/Avatar';
import '@fontsource/inter';
import Chip from '@mui/joy/Chip';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import ButtonGroup from '@mui/joy/ButtonGroup';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';
import CardActions from '@mui/joy/CardActions';
import IconButton from '@mui/joy/IconButton';
import Typography from '@mui/joy/Typography';
import SvgIcon from '@mui/joy/SvgIcon';

import { Link, useNavigate } from 'react-router-dom';

import { TextField } from '@mui/material';
import { useState } from 'react';

import { confirmAlert } from 'react-confirm-alert';
import { UserContext } from '../../ContextApi/ContexteApi';
import { Axios } from '../../Api/Axios';
import Copyright from '../../Footer/CopyRight';

export default function EditAdmin() {
  const {user}=UserContext()
  
  const [username,setUsername]=useState(user.name)
  const [email,setEmail]=useState(user.email)
  
  const navigate=useNavigate()
  const handleSubmit=async()=>{

await Axios.put(`/api/notifications/${user.id}`,{
 name:username,email
})
navigate('/AdminProfile')


  }
  const handleEditConfirmation = (e) => {
    e.preventDefault()
    confirmAlert({
      title: 'Edit Confirmation',
      message: 'Are you sure you want to edit?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            handleSubmit();
          }
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };
 
  return (<>
    <Card
      sx={{
        width: 600,
        height:600,
        maxWidth: '100%',
        boxShadow: 'lg',
        marginTop:"100px",
        marginRight:50,
        display:'flex',
        alignItems:'center',
        bgcolor:'#e9ecef47',
        border:'none',
        backdropFilter:'blur(50px)'
       
      }}
    >
      <CardContent sx={{ alignItems: 'center', textAlign: 'center' }}>
        <Avatar src="/static/images/avatar/1.jpg" sx={{ '--Avatar-size': '7rem' }} />
        <Chip
          size="m"
          variant="soft"
          color="primary"
          sx={{
            mt: -1,
            mb: 1,
            border: '1px solid',
            borderColor: 'background.surface',
          }}
        >
          Bienvenue
        </Chip>
      
        <TextField level="body-sm" sx={{ maxWidth: '34ch',width:400 }} type='text'onChange={(e)=>setUsername(e.target.value)} value={username} label="Username"  style={{marginBottom:"1px"}}/><br/>
        
       
        <TextField level="body-sm" sx={{ maxWidth: '34ch',width:400 }} type='text' onChange={(e)=>setEmail(e.target.value)} value={email} label="Email" style={{marginBottom:"1px"}}/>
       
        
        <Box
          sx={{
            display: 'flex',
            gap: 2,
            mt: 2,
            '& > button': { borderRadius: '2rem' },
          }}
        >
         
        </Box>
      </CardContent>
      <CardOverflow sx={{ bgcolor: 'background.level1' }}>
        <Link to={`/UserEdit/${user.id}`}><CardActions buttonFlex="1">
          <ButtonGroup variant="outlined" sx={{ bgcolor: 'background.surface',m:3 }}>
           <Button type='submit' onClick={handleEditConfirmation}>Modifier</Button>  
            
          </ButtonGroup>
        </CardActions></Link>
      </CardOverflow>
      
    </Card>
    
    </>
  );
}