import React from 'react'
import './reclamation.css';
import { Axios } from '../../Api/Axios';
import { useEffect, useState } from 'react';
import ReclamationApi from '../../ContextApi/ReclamationApi';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

const CardRec = () => {
    const [listReclamation,setReclamation]=useState([])
    const [currentPage, setCurrentPage] = useState(1);
    
    
    
    const ListReclamation=async(currentPage)=>{
        await Axios.get(`/api/reclamations?page=${currentPage}`).then((res)=>setReclamation(res.data.data))
    }
    useEffect(() => {
        
       ListReclamation(currentPage)
          
      }, [currentPage]);
  return (
    <div>
      <div className="cardcontent">
       {listReclamation.slice(1).map((e)=>{
        return (
          
            <div class="cardrec">
    <div class="cardrec-img"><img className='img' width={99} src='/258541.svg'/></div>
    <div class="cardrec-title">{e.ville}</div>
    <div class="cardrec-subtitle">{e.description}</div>
    <hr class="cardrec-divider"/>
    <div class="cardrec-footer">
        <div class="cardrec-price"></div>
        {e.status === 'En Cours' ? <button style={{background:'red'}} class="cardrec-btn">{e.status}</button>:<button style={{background:"#10B981"}} class="cardrec-btn">{e.status}</button>}
    </div>
</div>
          
        );
      })}</div>
      <Stack spacing={6} sx={{color:'white',alignItems:'center',marginTop:7,fontSize:40,fontWeight:'900'}}>
      <Pagination count={19} shape="rounded"  onChange={(e,value)=>setCurrentPage(value)}/>
      
    </Stack>
    </div>
  )
}

export default CardRec
