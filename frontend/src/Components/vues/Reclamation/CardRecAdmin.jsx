import React from 'react'
import './reclamation.css';
import { Axios } from '../../Api/Axios';
import { useEffect, useState } from 'react';
import ReclamationApi from '../../ContextApi/ReclamationApi';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { Link } from 'react-router-dom';
import 'react-confirm-alert/src/react-confirm-alert.css'; 
import { confirmAlert } from 'react-confirm-alert';
const CardRecAdmin = () => {
    const [listReclamation,setReclamation]=useState([])
    const [currentPage, setCurrentPage] = useState(1);
    
    
    
    const ListReclamation=async(currentPage)=>{
        await Axios.get(`/api/reclamations?page=${currentPage}`).then((res)=>setReclamation(res.data.data))
    }
    const Handledelete = async (id) => {
        confirmAlert({
          title: 'Delete',
          message: 'Are you sure you want to delete this item?',
          buttons: [
            {
              label: 'Yes',
              onClick: async () => {
                await ReclamationApi.deleteReclamation(id);
                ListReclamation(currentPage);
              }
            },
            {
              label: 'No',
              onClick: () => {}
            }
          ]
        });
      };
    useEffect(() => {
        
       ListReclamation(currentPage)
          
      }, [currentPage]);
  return (
    <div>
      <div className="cardcontent">
       {listReclamation.slice(1).map((e)=>{
        return (
          
            <div class="cardrec">
    <div class="cardrec-img"><img className='img' width={99} src='/258541.svg'/></div>
    <div class="cardrec-title">{e.ville}</div>
    <div class="cardrec-subtitle">{e.description}</div>
    <hr class="cardrec-divider"/>
    <div class="cardrec-footer">
        <div class="cardrec-price"></div>
        {e.status === 'En Cours' ? <Link to={`/EditReclamation/${e.id}`}><button style={{background:'red'}} class="cardrec-btn">{e.status}</button></Link>:<></>}
        <button className='cardrec-btn' onClick={()=>Handledelete(e.id)} style={{background:'red',color:'white'}}>Delete</button>

    </div>
</div>
          
        );
      })}</div>
      <Stack spacing={6} sx={{color:'white',alignItems:'start',marginTop:7,ml:4}}>
      <Pagination count={19} shape="rounded"  onChange={(e,value)=>setCurrentPage(value)}/>
      
    </Stack>
    </div>
  )
}

export default CardRecAdmin
