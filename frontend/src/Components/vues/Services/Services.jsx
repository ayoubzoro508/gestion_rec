import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import './services.css'
import FeedbackIcon from '@mui/icons-material/Feedback';
import AddCircleIcon from '@mui/icons-material/AddCircle';
const Services = () => {
    const navigate = useNavigate();

  return (
    <div className='services'>
        <div class="section_our_solution sec1">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="our_solution_category">
        <div class="solution_cards_box">
          <div class="solution_card">
            <div class="hover_color_bubble"></div>
            <div class="so_top_icon">
              <AddCircleIcon className='svg'/>
            </div>
            <div class="solu_title">
              <div>Reclamation</div>
            </div>
            <div class="solu_description">
              <p></p>
              <Link to={'/AddReclamation'}>
              <button class="read_more_btn" type="button">Ajouter</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section_our_solution sec2">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="our_solution_category">
        <div class="solution_cards_box">
          <div class="solution_card">
            <div class="hover_color_bubble"></div>
            <div class="so_top_icon">
              <AddCircleIcon className='svg'/>
            </div>
            <div class="solu_title">
              <div>Observation</div>
            </div>
            <div class="solu_description">
              <p></p>
              <Link to={'/AddObservation'}>
              <button class="read_more_btn" type="button">Ajouter</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section_our_solution sec3">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="our_solution_category">
        <div class="solution_cards_box">
          <div class="solution_card">
            <div class="hover_color_bubble"></div>
            <div class="so_top_icon">
              <AddCircleIcon className='svg'/>
            </div>
            <div class="solu_title">
              <div>Suggestion</div>
            </div>
            <div class="solu_description">
              <p></p>
              <Link to={'/AddSuggestion'}>
              <button class="read_more_btn" type="button">Ajouter</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
  )
}

export default Services
