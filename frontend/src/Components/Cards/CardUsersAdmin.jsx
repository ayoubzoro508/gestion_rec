import React, { useEffect, useState } from 'react'
import './card.css';
import { Link } from 'react-router-dom';
import { Pagination, Stack } from '@mui/material';
import { Axios } from '../Api/Axios';


import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'; 


const CardUsersAdmin = () => {
  const [listUsers,setUsers]=useState([])
  const [currentPage,setCurrentPage] = useState(1);
  const ListUsers=async(currentPage)=>{
      await Axios.get('/api/clients?page='+currentPage).then((res)=>setUsers(res.data.data))
  }
  const Handledelete = async (id) => {
    confirmAlert({
      title: 'Delete',
      message: 'Are you sure you want to delete this item?',
      buttons: [
        {
          label: 'Yes',
          onClick: async () => {
            await Axios.delete(`/api/clients/${id}`);
            ListUsers(currentPage);
          }
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };
  useEffect(() => {
    ListUsers(currentPage);
  }, [currentPage]);

    
  return (
    <div className='userdata'>
      <div className="usersdata">{
        listUsers.slice(1).map((e)=>{
          return (
            <>
            <div class="cardusr">
    <div class="cardusr-img"><img className='img' width={99} src='/person-button-svgrepo-com.svg'/></div>
    <div class="cardusr-title">{e.nom}</div>
    <div class="cardusr-subtitle">{e.email}</div>
    <div class="cardusr-subtitle">{e.telephone}</div>
    <div class="cardusr-subtitle">{e.created_at}</div>
    
    <hr class="cardusr-divider"/>
    <div class="cardusr-footer">
    <div class="cardusr-price"></div>
        <Link to={`/EditUsers/${e.id}`}><button style={{background:"#1070CA"}} className='cardusr-btn'>Edit</button></Link>
        <Link><button onClick={()=>Handledelete(e.id)} style={{background:'red'}} className='cardusr-btn'>Delete</button></Link>
    </div>
</div>
            </>
          );
        })
      }</div>
<Stack spacing={4} sx={{bgcolor:'transparent' ,mt:9,alignItems:"center"}}>
      <Pagination count={15} shape="rounded"  onChange={(e,value)=>setCurrentPage(value)}/>
      
    </Stack>
    </div>
  )
}

export default CardUsersAdmin
