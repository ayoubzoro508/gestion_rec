import React, { useEffect, useState } from 'react'
import './card.css'
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import AssignmentIcon from '@mui/icons-material/Assignment';
import PeopleAltSharpIcon from '@mui/icons-material/PeopleAltSharp';
import { Axios } from '../Api/Axios';
import Divider from '@mui/material/Divider';
const Card = (count) => {
  const [CountClient,setCountClient]=useState()
    const Countclients=async()=>{
   await Axios.get('/api/CountClient').then((res)=>setCountClient(res.data))
    }
    const [CountReclamation,setCountReclamation]=useState()
    const Countreclamations=async()=>{
   await Axios.get('/api/CountReclamation').then((res)=>setCountReclamation(res.data))
    }
    const [CountReclamationEncours,setCountReclamationEncours]=useState()
    const CountreclamationEncours=async()=>{
     await Axios.get('/api/CountReclamationEncours').then((res)=>setCountReclamationEncours(res.data))
      }
      const [CountReclamationTraité,setCountReclamationTraité]=useState()
      const CountreclamationTraité=async()=>{
       await Axios.get('/api/CountReclamationTraité').then((res)=>setCountReclamationTraité(res.data))
        }
     useEffect(()=>{
       Countclients();
       Countreclamations()
       CountreclamationEncours()
       CountreclamationTraité()
     },[])
  return (
    <div className='cards'>
      
        <div className='cardtop'>
        <div className="card c1">
          <div className="icons icon1">
          <AssignmentTurnedInIcon fontSize='large'/>
          </div>
          <div className='card-content'>
            {CountReclamationTraité}
          </div>
          <Divider sx={{position:"relative",top:-50,width:'86%',ml:3}}/>
          <div className='card-footer'>
            <p>Reclamation Traitée</p>
          </div>
        </div>
        <div className="card c2">
          <div className="icons icon2">
          <AccessTimeIcon fontSize='large'/>
          </div>
          <div className='card-content'>
            {CountReclamationEncours}
          </div>
          <Divider sx={{position:"relative",top:-50,width:'86%',ml:3}}/>
          <div className='card-footer'>
          <p>Reclamation En Cours</p>
          </div>
        </div>
        </div>
        <div className="cardbuttom">
        <div className="card c3">
          <div className="icons icon3">
          <AssignmentIcon  fontSize='large'/>
          </div>
          <div className='card-content'>
            {CountReclamation}
          </div>
          <Divider sx={{position:"relative",top:-50,width:'86%',ml:3}}/>
          <div className='card-footer'>
          <p>Reclamations</p>
          </div>
        </div>
        <div className="card c4">
          <div className="icons icon4">
          <PeopleAltSharpIcon className='icon icon4' fontSize='large'/>
          </div>
          <div className='card-content'>
            {CountClient}
          </div>
          <Divider sx={{position:"relative",top:-50,width:'86%',ml:3}}/>
          <div className='card-footer'>
          <p>Users</p>
          </div>
        </div>
        </div>
     
      
    </div>
  )
}

export default Card
