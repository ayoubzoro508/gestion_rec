import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Axios } from '../../Api/Axios';
import { Alert } from '@mui/material';




// TODO remove, this demo shouldn't need to reset the theme.

const defaultTheme = createTheme();

export default function Register() {
  const[errorEmail,setErrorEmail]=React.useState()
  const[errorTelephone,setErrorTelephone]=React.useState()
  const[errorAdresse,setErrorAdresse]=React.useState()
  const[errorPrenom,setErrorPrenom]=React.useState()
  const[errorNom,setErrorNom]=React.useState()
  const[errorPass,setErrorPass]=React.useState()
  const[success,setSuccess]=React.useState()
  const handleSubmit = async(event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const values={
      nom:data.get('firstName'),
      prenom:data.get('lastName'),
      email: data.get('email'),
      password: data.get('password'),
      // confirmation_password: data.get('password_confirmation'),
      telephone:data.get('telephone'),
      adresse:data.get("adresse")
  }

    try {
      await Axios.post('/register',values)
     
      setSuccess('User added successfully!')
    } catch (error) {
      setErrorEmail(error.response.data.errors.email);
      setErrorNom(error.response.data.errors.nom);
      setErrorPrenom(error.response.data.errors.prenom);
      setErrorAdresse(error.response.data.errors.adresse);
      setErrorPass(error.response.data.errors.password);
      setErrorTelephone(error.response.data.errors.telephone);
    }
    }

  return (
    
      <Container component="main" maxWidth="xs"
        
      >

        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
         
          
          <Box component="form" className='form_main' noValidate onSubmit={handleSubmit} sx={{ mt: 9,color:"white" }}>
          <Typography component="h1" variant="h5" className='heading'>
            Register
          </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                /><span style={{color:"red"}}>{errorPrenom}</span>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                />
                <span style={{color:"red"}}>{errorNom}</span>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
                <span style={{color:"red"}}>{errorEmail}</span>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                /> <span style={{color:"red"}}>{errorPass}</span>
              </Grid>
             
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="telephone"
                  label="telephone"
                  type="telephone"
                  id="telephone"
                  autoComplete="new-telephone"
                />
                <span style={{color:"red"}}>{errorTelephone}</span>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="adresse"
                  label="Adresse"
                  type="adresse"
                  id="adresse"
                  
                />
                <span style={{color:"red"}}>{errorAdresse}</span>
              </Grid>

              
            </Grid><br/>{     success?   <Alert>{success}</Alert>:null}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
               
              </Grid>
            </Grid>
          </Box>
        </Box>
       
      </Container>
   
  );
}

