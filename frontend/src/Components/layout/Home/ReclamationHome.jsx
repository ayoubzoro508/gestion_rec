import React, { useState } from 'react'

import SidebarAdmin from '../../Sidebar/User/SidebarHome'
import './style.css'
import Card from '../../Cards/Card'
import Calendar from '../../vues/Calendar/Calendar'
import Reclamation from '../../vues/Reclamation/Reclamation'
import Client from '../../vues/Client/Client'
import Chart from '../../vues/Chart/Chart1/Chart1'
import Chart2 from '../../vues/Chart/Chart2/Chart2'
import SidebarHome from '../../Sidebar/User/SidebarHome';
import Login from '../../Form/Login/Login';
import { Search, SearchOutlined } from '@mui/icons-material';
import { Divider, TextField } from '@mui/material';

export default function ReclamationHome() {
  const  [search,setSearch]=useState('')
  return (
    <div className='rec'>
      <div className="navigation">
      <SidebarHome/>
     
      </div>
     
      <div className='homeContainer'>
    <div className='search'>  <TextField type="search" style={{width:"400px"}} onChange={(e)=>setSearch(e.target.value)}  label="Search"/></div>
    <Divider sx={{
      width:1000,
      marginLeft:10
    }}/>
        <div className='homeRec'>
          <Reclamation search={search}/>
          
          
        </div>
      </div>
    </div>
  )
}
