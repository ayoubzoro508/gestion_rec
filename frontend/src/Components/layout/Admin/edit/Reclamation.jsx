
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

import { pink } from '@mui/material/colors';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import {useNavigate, useParams} from "react-router-dom"
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { MenuItem, Select, TextareaAutosize } from '@mui/material';
import '../style.css'
import { useEffect, useState } from 'react';
import { UserContext } from '../../../ContextApi/ContexteApi';
import { Axios } from '../../../Api/Axios';
import ReclamationApi from '../../../ContextApi/ReclamationApi';
import { Edit } from '@mui/icons-material';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Chart2 from './../../../vues/Chart/Chart2/Chart2';

// TODO remove, this demo shouldn't need to reset the theme.

const defaultTheme = createTheme();

export default function Reclamation() {
  const [ville,setVille]=useState([])
  const [Description,setDescription]=useState([])
  const [status,setStatus]=useState([])
  const [checked,setChecked] = useState(true);
  let navigate=useNavigate()
  const {id}=useParams();
  const handleShow=async(id)=>{
      await ReclamationApi.getReclamation(id).then(({data})=>{
        setVille(data.ville)
        setDescription(data.description)
        setStatus(data.status)
      })
}
const {user}=UserContext()
const handleEditConfirmation = (e) => {
  e.preventDefault()
  confirmAlert({
    title: 'Edit Confirmation',
    message: 'Are you sure you want to edit?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          EditShow();
        }
      },
      {
        label: 'No',
        onClick: () => {}
      }
    ]
  });
};
const EditShow=async()=>{
    

    
    const values={
    
    ville,
    description:Description,
    status: status?"Traitée":"En Cours"
    }


 console.log(values)
    await Axios.put(`/api/reclamations/${id}`,values)
navigate('/AdminReclamation')
  }     

  useEffect(()=>{
      handleShow(id)},[])

  
  
   // axios.get('ma.json').then((res)=>setVille(res.data))
  
//     const handleSubmit = async(event) => {
//     event.preventDefault();
//     const data = new FormData(event.currentTarget);
// const values={
//       ville: data.get('ville'),
//       description: data.get('description'),
//       status: data.get('status'),
//       idUser:user.id

//     }
//     await Axios.put("api/reclamations" ,values)
// navigate('/reclamationUser')
//   };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        
        
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            justifyContent:'center',
            alignItems: 'center',
            
          }}
        >
          
          <Box component="form" noValidate  onSubmit={handleEditConfirmation} sx={{
            borderRadius:2,
            
            width:700,
            height:600,
            display:'flex',
            flexDirection:'column',
            alignItems:"center",
            justifyContent:'center',
            
            backdropFilter:"blur(5px)",
            marginLeft:-60
          }}> 
            <Grid container spacing={4} sx={{
              display:'flex',
              justifyContent:'center',
              alignItems:'center'
            }}>
             
              <Grid item xs={8}>
                <TextField
                  required

                  id="ville"
                  type='text'
                  label="Ville"
                  name="ville"
                  placeholder='Selectionner un Ville'
                  onChange={(e)=>setVille(e.target.value)}
                value={ville}
            sx={{width:350,marginLeft:3}}
            
                  
                  
               />
                  
               
              </Grid><br /><br />
              <Grid item xs={8}>
              <TextareaAutosize required  aria-label="minimum height" label="Description"  name={"description"}minRows={3} cols={70}   onChange={(e)=>setDescription(e.target.value)}
                value={Description}placeholder=""
                 
                 />
              </Grid>
              <Grid item xs={8} margin={3}>
              <FormControlLabel 
                control={status === 'En Cours'?<Checkbox  onChange={(()=>setStatus(e.target.checked))}/>:<Checkbox color='success' onChange={(()=>setStatus(e.target.checked))} checked/>}
                label={status}
              />
              
              </Grid>
              
              
            </Grid>
           <Grid xs={8}>
           <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2,bgcolor:'#5603ad',width:350 }}
             
            >
              Traitée
            </Button>
           </Grid>
            
          </Box>
        </Box>
       
      </Container>
    </ThemeProvider>
  );
}

