import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import Parametre from '../../vues/Parametre/Parametre';
import Reclamation from '../../vues/Reclamation/Reclamation';
import SidebarAdmin from '../../Sidebar/Admin copy/SidebarAdmin';
import ListReclamation from './ListReclamation';
import ListObservation from './ListObservation';
export default function AdminObservation() {
  return (
    <div className='home'>
      <div className="navigation">
      <SidebarAdmin/>
      
      </div>
      
        
        <div className='homeContent' style={{marginTop:200}}>
       <ListObservation/>
          
          
        </div>
      
    </div>
  )
}
