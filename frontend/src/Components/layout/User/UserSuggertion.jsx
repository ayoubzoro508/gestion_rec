import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import Parametre from '../../vues/Parametre/Parametre';
import UserSuggestions from './user/UserSuggestion';
import Button from '@mui/joy/Button';
import { Link } from 'react-router-dom';

export default function UserSurggestion() {
  return (
    <div className='home'>
      <div className="navigation">
      <SidebarGuest/>
      
      </div>
      
      <div className='homecontainer' >
        <div className='Contentuser'>
        <Link to="/AddSuggestion"><Button style={{marginBottom:"10px",marginLeft:"200px" ,marginTop:"150px"}}>Nouvelle Suggestion</Button></Link>

        <UserSuggestions/>
          {/* <div className="top">
          <Card id='cardsh'/>
          <Calendar id="calendarh"/>
          
          </div>
          <div className="midle">
            <Chart2 id="chart2h"/>
            <Chart id="charth"/>
            
          </div> */}
          {/* <div className="buttom">
            <h1>List des Reclamations</h1>
            <Reclamation className="reclamationh"/>
            <h1>List des Clients</h1>
            <Client className="clienth"/>
          </div> */}
          
        </div>
      </div>
      
    </div>
  )
}
