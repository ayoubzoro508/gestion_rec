import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import Parametre from '../../vues/Parametre/Parametre';
import Reclamation from '../../vues/Reclamation/Reclamation';
import UserReclamations from './user/UserReclamation';
import Button from '@mui/joy/Button';
import { Link } from 'react-router-dom';

export default function UserReclamation() {
  return (
    <div className='homeuser'>
      <div className="navigation">
      <SidebarGuest/>
      
      </div>
      <div className='homecontaineruser' >
        <div className='Contentuser'>
        <Link to="/AddReclamation">  <Button style={{marginBottom:"10px",marginLeft:"72px"}}>Nouvelle Reclamation</Button></Link>

        <UserReclamations/>
          
          
        </div>
      </div>
      
    </div>
  )
}
