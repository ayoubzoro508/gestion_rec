import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import Parametre from '../../vues/Parametre/Parametre';
import UserSuggestions from './user/UserSuggestion';
import Button from '@mui/joy/Button';
import UserObservations from './user/UserObservations';
import { Link } from 'react-router-dom';



export default function UserObservation() {
  return (
    <div className='home'>
      <div className="navigation">
      <SidebarGuest/>
      
      </div>
      
      <div className='homecontaineruser' style={{marginBottom:"420px"}}>
        
        <div className='Contentuser'>

          <Link to={"/AddObservation"}><Button style={{marginBottom:"10px",marginLeft:"72px"}}>Nouvelle Observation</Button></Link>
            <UserObservations/>
          
          
        </div>
      </div>
      
    </div>
  )
}
