import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import { UserContext } from '../../ContextApi/ContexteApi';
import BioCard from '../../Cards/Carduser';

export default function UserProfile() {
  
  return (
    <div className='homeuser'>
      <div className="navigation">
      <SidebarGuest/>
      
      </div>
      <div className='homecontaineruser'>
        
        <div className='homeContent'>
         
        <BioCard />
         
          
        </div>
      </div>
      
    </div>
  )
}
