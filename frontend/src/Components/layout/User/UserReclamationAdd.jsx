import React from 'react'


import SidebarGuest from '../../Sidebar/Guest/SidebarGuest';
import Parametre from '../../vues/Parametre/Parametre';
import UserSuggestions from './user/UserSuggestion';
import Button from '@mui/joy/Button';
import UserObservations from './user/UserObservations';
import { Link } from 'react-router-dom';
import AddObservation from './user/Actions/AddObservation';
import AddReclamation from './user/Actions/AddReclamation';



export default function UserReclamationAdd() {
  return (
    <div className='home'>
      <div className="navigation">
      <SidebarGuest/>
      
      </div>
      
      <div className='homecontaineruser' >
        
        <div className='homeContent'>
        <AddReclamation/>
          
          
        </div>
      </div>
      
    </div>
  )
}
