import React, { useEffect, useState } from 'react'
import './home.css';


import Card from '../Cards/Card'
import Calendar from '../vues/Calendar/Calendar'
import Reclamation from '../vues/Reclamation/Reclamation'
import Client from '../vues/Client/Client'
import Chart from '../vues/Chart/Chart1/Chart1'
import Chart2 from '../vues/Chart/Chart2/Chart2'
import SidebarGuest from '../Sidebar/Guest/SidebarGuest';
import { UserContext } from '../ContextApi/ContexteApi';
import { Link, useNavigate } from 'react-router-dom';
import ReclamationApi from '../ContextApi/ReclamationApi';

import { Button } from '@mui/material';
import CardRec from '../vues/Reclamation/CardRec';
import Services from '../vues/Services/Services';
export default function UserHome() {
  
  const scroll = () => {
    window.scrollTo({top:800,behavior:'smooth'})
  }
  return (
    <div className='homeuser'>
      <div className="navigation">
      <SidebarGuest/>
      
      
      </div>
      <div className='homecontaineruser'>
        
        <div className='homeContentuser'>
         <div className="headeruser">
         <div className="user">
         <div class="content">
  <div class="content__container">
    <div className='span' class="content__container__text">
      BONJOUR
    </div>
    
    <ul class="content__container__list">
      <li class="content__container__list__item">Le monde !</li>
      <li class="content__container__list__item">Visiteurs !</li>
      <li class="content__container__list__item">utilisateurs !</li>
      <li class="content__container__list__item">tout le monde !</li>
    </ul>
  </div>
</div>
          <h1>Gestion des <div class="focus"> 
  <div class="focus--mask">
    <div class="focus--mask-inner">Reclamations</div>
  </div>
</div></h1>
      <span>Développment Digital Option Web Full Stack</span>
      <div className="btns">
          <Button className='btnuser'  onClick={scroll}
          sx={{bgcolor:'#14B8A6',
                
                borderRadius:0,
                borderBottom:"2px solid white",
                color:"white",
                marginTop:15,
                height:50,
                paddingTop:3,
                paddingLeft:5,
                paddingRight:5,
                paddingBottom:3,
                fontSize:1

          }}><span>Afficher plus</span></Button>
          
          </div>
      </div>
      
         </div>
          <div className="topuser">
          <Card id='cardsh'/>
          
          
          </div>
          <div className="midleuser">
            <Chart2 id="chart2h"/>
            <Chart id="charth"/>
            
          </div>
          <div className="footeruser">
            <h1>Services</h1>
            <Services/>
            <h1>Reclamations</h1>
            <CardRec/>
          </div>
          
        </div>
      </div>
      
    </div>
  )
}
