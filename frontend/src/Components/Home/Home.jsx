import React, { useEffect, useState } from 'react'
import './home.css';
import SidebarAdmin from '../Sidebar/User/SidebarHome'
import Card from '../Cards/Card'
import Calendar from '../vues/Calendar/Calendar'
import Reclamation from '../vues/Reclamation/Reclamation'
import Client from '../vues/Client/Client'
import Chart from '../vues/Chart/Chart1/Chart1'
import Chart2 from '../vues/Chart/Chart2/Chart2'
import SidebarHome from '../Sidebar/User/SidebarHome';
import { Axios } from '../Api/Axios';
import Copyright from '../Footer/CopyRight';
import { Button, Hidden } from '@mui/material';
import { Link } from 'react-router-dom';
import CardRec from '../vues/Reclamation/CardRec';
export default function Home() {
  
    const [CountClient,setCountClient]=useState()
    const Countclients=async()=>{
   await Axios.get('/api/CountClient').then((res)=>setCountClient(res.data))
    }
    const [CountReclamation,setCountReclamation]=useState()
    const Countreclamations=async()=>{
   await Axios.get('/api/CountReclamation').then((res)=>setCountReclamation(res.data))
    }
    const [CountReclamationEncours,setCountReclamationEncours]=useState()
    const CountreclamationEncours=async()=>{
     await Axios.get('/api/CountReclamationEncours').then((res)=>setCountReclamationEncours(res.data))
      }
      const [CountReclamationTraité,setCountReclamationTraité]=useState()
      const CountreclamationTraité=async()=>{
       await Axios.get('/api/CountReclamationTraité').then((res)=>setCountReclamationTraité(res.data))
        }
     useEffect(()=>{
       Countclients();
       Countreclamations()
       CountreclamationEncours()
       CountreclamationTraité()
     },[])

     const scroll = () => {
      window.scrollTo({top:800,behavior:"smooth"})
     }
  return (
    <div className='homeuser'>
      <div className="navigation">
      <SidebarHome/>
     
      </div>
     <div className="headeruser">
      <div className="user">
      <div class="content">
  <div class="content__container">
    <div className='span' class="content__container__text">
      BONJOUR
    </div>
    
    <ul class="content__container__list">
      <li class="content__container__list__item">Le monde !</li>
      <li class="content__container__list__item">Admin !</li>
      <li class="content__container__list__item">utilisateurs !</li>
      <li class="content__container__list__item">tout le monde !</li>
    </ul>
  </div>
</div>
        <h2 className='titre'>GESTION</h2> <div class="focus"> 
  <div class="focus--mask">
    <div class="focus--mask-inner">Reclamations</div>
  </div>
</div>

      <div className='soustitre'>Développment Digital Option Web Full Stack</div>
      <div className="btns">
          <Button className='btnuser' disableRipple='true' onClick={scroll}
          sx={{bgcolor:'#14B8A6',
                
                borderRadius:0,
                borderBottom:"2px solid white",
                color:"white",
                marginTop:15,
                height:50,
                paddingTop:3,
                paddingLeft:5,
                paddingRight:5,
                paddingBottom:3,

          }}><span>Afficher plus</span></Button>
          <Link to='/Login'>
          <Button className='btnuser' disableRipple onClick={scroll}
          sx={{bgcolor:'#1070CA',
                
                borderRadius:0,
                borderBottom:"2px solid ",
                paddingTop:3,
                paddingLeft:8,
                paddingRight:8,
                paddingBottom:3,
                color:"white",
                marginTop:15,
                marginLeft:12,
                height:50,
                

          }}><span>Login</span></Button>
          </Link>
          </div>
          </div>
     </div>
      <div className='homecontaineruser'>
        
        <div className='homecontentuser'>
          <div className="topuser">
          <h1>Statistiques</h1>
          <Card id='cardsh'/>
          
          
          </div>
          <div className="midleuser">
            
            <Chart/>
            <Chart2 />
          </div>
          <div className="footeruser">
            <h1>List Des Reclamations</h1>
            <CardRec/>
            
          </div>
          
        </div>
      </div>
      
    </div>
  )
}
