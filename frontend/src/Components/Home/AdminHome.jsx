import React from 'react'
import './Admin.css';
import SidebarAdmin from '../Sidebar/Admin copy/SidebarAdmin'

import Card from '../Cards/Card'
import Calendar from '../vues/Calendar/Calendar'
import Reclamation from '../vues/Reclamation/Reclamation'
import Client from '../vues/Client/Client'
import Chart from '../vues/Chart/Chart1/Chart1'
import Chart2 from '../vues/Chart/Chart2/Chart2'
import ListUsers from '../layout/Admin/ListUsers';
import ListUsers1 from '../layout/Admin/ListUsers copy';
import Copyright from '../Footer/CopyRight';
import CardRec from './../vues/Reclamation/CardRec';
import CardUsersAdmin from '../Cards/CardUsersAdmin';
import CardRecAdmin from '../vues/Reclamation/CardRecAdmin';
export default function AdminHome() {
  return (
    <div className='home'>
      <div className="navigation">
      <SidebarAdmin/>
      
      </div>
      <div className='homecontaineradmin'>
        
        <div className='homeContentadmin'>
          <div className="top">
            
          <Card id='cardsh'/>
          
          
          </div>
          <div className="midle">
            <Chart2 id="chart2h"/>
            <Chart id="charth"/>
            
          </div>
          <div className="buttom">
            <h1 style={{marginLeft:"40px"}}>List des Reclamations</h1>
            <CardRecAdmin/>
            <h1 style={{marginLeft:"40px"}}>List des Utilisateurs</h1>
            <CardUsersAdmin/>
          </div>
          
        </div>
      </div>
    
    </div>
  )
}
