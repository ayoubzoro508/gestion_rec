
import Typography from '@mui/material/Typography'

import Link from '@mui/material/Link';

export default function Copyright(props) {
    return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}
      sx={{
        bgcolor:'#111827',
        color:"#fff",
        height:60,
        fontSize:17,
        display:"flex",
        alignItems:"center",
        justifyContent:"center"

      }}
      >
        {'Copyright © '}
        <Link color="inherit" href="#">
          DDOWFS 201 
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }