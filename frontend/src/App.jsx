import { BrowserRouter, Route, RouterProvider, Routes } from 'react-router-dom'
import './App.css'



import { Index } from './Components/Index'

import ReclamationContext from './Components/ContextApi/ContexteApi'

function App() {
  

  return (
    <>
     
     <ReclamationContext>
      <RouterProvider router={Index}></RouterProvider>
    </ReclamationContext>
    </>
  )
}

export default App
